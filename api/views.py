from django.shortcuts import render
from django.views import View
from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .models import Movie, Rating
from rest_framework.decorators import action
from .serializers import MovieSerializer, RatingSerializer, UserSerializer
from django.contrib.auth.models import User

# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

class MovieViewSet(viewsets.ModelViewSet):
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    @action(detail=True, methods=['POST',])
    def rate_movie(self, request, pk=None,):
        if 'stars' in request.data:
            print(pk)
            stars = request.data['stars']
            movie = Movie.objects.get(id=pk)
            #user = User.objects.get(id=1)
            user = request.user
            #print('user', user.username)
            print('user ', user)
            print("movie title:", movie.title)
            try:
                rating = Rating.objects.get(user=user.id, movie=movie.id, )
                rating.stars = stars
                rating.save()
                serializer = RatingSerializer(rating, many=False, )
                response = {'message': 'Rating updated ', 'result': serializer.data, }
                return Response(response, status=status.HTTP_200_OK)
            except:
                rating = Rating.objects.create(user=user, movie=movie, stars=stars, )
                serializer = RatingSerializer(rating, many=False, )
                response = {'message': 'Rating created ', 'result': serializer.data, }
                return Response(response, status=status.HTTP_200_OK)
        else:
            response = {'message': 'you need to provide stars', }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class RatingViewSet(viewsets.ModelViewSet):
    serializer_class = RatingSerializer
    queryset = Rating.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        response = {'message': 'you cannot update by default method', }
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        response = {'message': 'you cannot create by default method', }
        return Response(response, status=status.HTTP_400_BAD_REQUEST)
